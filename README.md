# Sql2Laravel

[![Latest Version on Packagist](https://img.shields.io/packagist/v/itul/sql-to-laravel.svg?style=flat-square)](https://packagist.org/packages/itul/sql-to-laravel)
[![Total Downloads](https://img.shields.io/packagist/dt/itul/sql-to-laravel.svg?style=flat-square)](https://packagist.org/packages/itul/sql-to-laravel)
![GitHub Actions](https://github.com/itul/sql-to-laravel/actions/workflows/main.yml/badge.svg)

A simple package to convert raw sql query strings to Laravel Query Builder objects. 

See Full documentation at (https://laraveltesting.itulbuild.com/documentation/sql-to-laravel)

## Installation

You can install the package via composer:

```bash
composer require itul/sql-to-laravel
```

## Usage

```php
$sql = "SELECT * FROM users";

//THE $obj VARIABLE WILL TRY TO PARSE THE SQL AND CONVERT IT TO A LARAVEL QUERY BUILDER OBJECT
$obj = \Itul\SqlToLaravel\SqlToLaravel::convert($sql);

//ARBRITRARY WHERE STATEMENT AS EXAMPLE (ANY LARAVEL QUERY BUILDER METHODS CAN BE USED HERE)
$obj->where('status', true);

//PRINT OUT THE RESULTS
dd($obj->get());
```
### Changelog

Please see [CHANGELOG](CHANGELOG.md) for more information what has changed recently.

## Contributing

Please see [CONTRIBUTING](CONTRIBUTING.md) for details.

### Security

If you discover any security related issues, please email brandon@i-tul.com instead of using the issue tracker.

## Credits

-   [Brandon Moore](https://github.com/itul)
-   [All Contributors](../../contributors)


