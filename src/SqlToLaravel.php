<?php

namespace Itul\SqlToLaravel;

class SqlToLaravel
{
    
    public static function convert(string $sql){
        $converter      =  new \RexShijaku\SQLToLaravelBuilder\SQLToLaravelBuilder(['facade' => '\\DB::']);
        $fileName       = md5(microtime(true).rand(0,9999)).'.php';
        $content        = '<?php return '.substr($converter->convert($sql), 0, -8).';';
        \Illuminate\Support\Facades\Storage::disk('local')->put('temp/'.$fileName, $content);
        $path           = \Illuminate\Support\Facades\Storage::disk('local')->path('temp/'.$fileName);
        $queryObject    = include_once $path;
        unlink($path);
        return $queryObject;
    }
}
