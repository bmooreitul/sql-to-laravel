<?php

namespace Itul\SqlToLaravel;

use Illuminate\Support\Facades\Facade;

/**
 * @see \Itul\SqlToLaravel\Skeleton\SkeletonClass
 */
class SqlToLaravelFacade extends Facade
{
    /**
     * Get the registered name of the component.
     *
     * @return string
     */
    protected static function getFacadeAccessor()
    {
        return 'sql-to-laravel';
    }
}
